build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  script:
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/hello:gitlab-$CI_COMMIT_SHORT_SHApython -f Dockerfile .
    - docker push $CI_REGISTRY/hello:gitlab-$CI_COMMIT_SHORT_SHA